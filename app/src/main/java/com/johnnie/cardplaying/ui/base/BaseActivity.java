package com.johnnie.cardplaying.ui.base;

import android.support.v4.app.Fragment;

import com.johnnie.cardplaying.R;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * Created by Johnnie on 18/07/18.
 */

public class BaseActivity extends DaggerAppCompatActivity {

    private static final String TAG_HOST_FRAGMENT = "tag_hostFragment";

    protected void replace(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(0, 0)
                .replace(R.id.host_container, fragment, TAG_HOST_FRAGMENT)
                .commit();
    }

    @Override
    public void onBackPressed() {
        boolean hasHandle = false;

        Fragment currentHostFragment = getSupportFragmentManager().findFragmentByTag(TAG_HOST_FRAGMENT);
        if(currentHostFragment instanceof Backable){
            hasHandle =((Backable)currentHostFragment).onBackClicked();
        }

        if(!hasHandle){
            super.finish();
        }
    }

}

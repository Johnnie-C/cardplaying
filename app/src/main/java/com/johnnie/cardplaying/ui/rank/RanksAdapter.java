package com.johnnie.cardplaying.ui.rank;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.ScoreRank;
import com.johnnie.cardplaying.ui.base.BaseRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Johnnie on 18/07/18.
 */

public class RanksAdapter extends BaseRecyclerViewAdapter<ScoreRank> {

    private final static int VIEW_TYPE_RANK_CELL = 0;
    private final static int VIEW_TYPE_EMPTY_CELL = 1;

    private ArrayList<ScoreRank> ranks;

    public void setRanks(ArrayList<ScoreRank> ranks) {
        this.ranks = ranks;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType){
            case VIEW_TYPE_RANK_CELL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rank, parent, false);
                viewHolder = new RankCell(view);
                break;

            case VIEW_TYPE_EMPTY_CELL:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_rank, parent, false);
                viewHolder = new RankEmptyCell(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return ranks.size() == 0 ? VIEW_TYPE_EMPTY_CELL : VIEW_TYPE_RANK_CELL;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof RankCell) {
            RankCell cell = (RankCell) holder;
            ScoreRank rank = ranks.get(position);

            cell.txtRank.setText(String.valueOf(position + 1));
            cell.txtName.setText(rank.getName());
            cell.txtScore.setText(String.valueOf(rank.getScore()));

        }
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if(ranks != null){
            count = ranks.size() <= 0 ? 1 : ranks.size();
        }
        return count;
    }





    public class RankCell extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_rank) TextView txtRank;
        @BindView(R.id.txt_name) TextView txtName;
        @BindView(R.id.txt_score) TextView txtScore;

        public RankCell(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }





    public class RankEmptyCell extends RecyclerView.ViewHolder{

        public RankEmptyCell(View itemView) {
            super(itemView);
        }

    }
}

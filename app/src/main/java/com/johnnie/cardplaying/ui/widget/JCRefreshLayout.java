package com.johnnie.cardplaying.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

import com.johnnie.cardplaying.R;


/**
 * Created by Johnnie on 09/05/18.
 */

public class JCRefreshLayout extends SwipeRefreshLayout {

    private static final double MIN_REFRESHING_PERIOD = 1000 * 0.8; //0.8 second
    private long refreshingStartTime;

    private OnRefreshListener fphListener;


    public JCRefreshLayout(@NonNull Context context) {
        super(context);
        init();
    }

    public JCRefreshLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setColorSchemeResources(R.color.cp_blue, R.color.cp_blue_lighter, R.color.cp_blue_darker);
    }

    /**
     * Deprecated for external use
     * use {@link JCRefreshLayout#startLoading} and {@link JCRefreshLayout#stopLoading} instead
     *
     * ensure refresh will display for a certain period {@link JCRefreshLayout#MIN_REFRESHING_PERIOD}
     * above methods prevent user from seeing flash on loader when the loading time is extremely short
     *
     * @param refreshing
     */
    @Override
    @Deprecated
    public void setRefreshing(boolean refreshing){
        super.setRefreshing(refreshing);
    }

    public void startLoading() {
        refreshingStartTime = System.currentTimeMillis();
        if(isRefreshing()) return;
        post(() -> setRefreshing(true));
    }

    public void stopLoading() {
        postDelayed(() -> {
            if(getRemainRefreshingTime() > 0){
                stopLoading();
            }
            else{
                stopLoadingImmediately();
            }

        }, getRemainRefreshingTime());
    }

    public void stopLoadingImmediately() {
        setRefreshing(false);
    }

    private long getRemainRefreshingTime(){
        return (long)Math.max(0, (MIN_REFRESHING_PERIOD - (System.currentTimeMillis() - refreshingStartTime)));
    }

    @Override
    public void setOnRefreshListener(@Nullable OnRefreshListener listener) {
        fphListener = listener;
        super.setOnRefreshListener(() -> {
            refreshingStartTime = System.currentTimeMillis();
            if(fphListener != null){
                fphListener.onRefresh();
            }
        });
    }
}

package com.johnnie.cardplaying.ui.base;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Johnnie on 18/07/18.
 */

public abstract class BaseRecyclerViewAdapter<T> extends RecyclerView.Adapter {

    protected ListItemClickedListener<T> itemClickedListener;

    public void setItemClickedListener(ListItemClickedListener<T> itemClickedListener) {
        this.itemClickedListener = itemClickedListener;
    }

}

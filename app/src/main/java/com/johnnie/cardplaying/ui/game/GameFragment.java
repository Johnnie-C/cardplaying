package com.johnnie.cardplaying.ui.game;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.ScoreRank;
import com.johnnie.cardplaying.modle.card.Card;
import com.johnnie.cardplaying.modle.game.Game;
import com.johnnie.cardplaying.modle.game.MatchResult;
import com.johnnie.cardplaying.presenter.GamePresenter;
import com.johnnie.cardplaying.ui.base.Backable;
import com.johnnie.cardplaying.ui.base.BaseFeatureFragment;
import com.johnnie.cardplaying.ui.gameList.SpaceItemDecoration;
import com.johnnie.cardplaying.ui.rank.RankFragment;
import com.johnnie.cardplaying.utils.SimpleDisposableObserver;
import com.johnnie.cardplaying.utils.UIUtils;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

import static android.graphics.PorterDuff.Mode.SRC_IN;

/**
 * Created by Johnnie on 18/07/18.
 */

public class GameFragment extends BaseFeatureFragment implements GamePresenter.GameView,
        GameAdapter.GameAdapterListener, Backable{

    private final static String KEY_GAME = "key_game";
    private final static String KEY_CARDS = "key_cards";
    private final static String KEY_SELECTED_CARDS = "key_selectedCards";
    private final static String KEY_SCORE = "key_score";
    private final static String KEY_RESTORE_NAME_INPUT_DIALOG = "key_restoreNameInputDialog";
    private final static String KEY_PENDING_MATCH_RESULT = "key_pendingMatchResult";

    @BindView(R.id.game_board) RecyclerView recyclerView;
    private GameAdapter adapter;

    @Inject GamePresenter presenter;

    private Game<Card> game;
    private ArrayList<Card> cards;
    private ArrayList<Card> selectedCards;
    private int score;
    private boolean shouldRestoreNameInputDialog;
    private MatchResult pendingMatchResult;
    private Disposable matchResultDisposable;


    public static GameFragment newInstance(Game game){
        GameFragment fragment = new GameFragment();

        Bundle args = new Bundle();
        args.putParcelable(KEY_GAME, game);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_game_board;
    }

    @Override
    protected void viewDidLoad(View rootView, Bundle savedInstanceState){
        super.viewDidLoad(rootView, savedInstanceState);
        initData(savedInstanceState);
        setupPresenter();
        setupView();

        if(cards == null || cards.size() <= 0){
            presenter.prepareCards(game);
        }
        else{
            onCardsPrepared(cards);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_GAME, game);
        outState.putParcelableArrayList(KEY_CARDS, cards);
        outState.putParcelableArrayList(KEY_SELECTED_CARDS, selectedCards);
        outState.putInt(KEY_SCORE, score);
        outState.putBoolean(KEY_RESTORE_NAME_INPUT_DIALOG, shouldRestoreNameInputDialog);
        outState.putParcelable(KEY_PENDING_MATCH_RESULT, pendingMatchResult);
    }

    @Override
    public void onResume(){
        super.onResume();
        setTitle(game.getNameRes());
        updateScore();

        if(shouldRestoreNameInputDialog && game != null && cards != null){
            onFinishGame(game.hasWin(cards));
        }
    }

    @Override
    protected void onToolbarReady(@Nullable Toolbar toolbar){
        if(toolbar == null) return;

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(view -> getActivity().onBackPressed());
    }

    @Override
    protected boolean hasOwnMenu(){
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.game_menu, menu);

        //Change icon color to white
        //TODO: remove this after find a white rank icon
        MenuItem item = menu.findItem(R.id.action_rank);
        if(item != null){
            Drawable drawable = item.getIcon();
            drawable.setColorFilter(getContext().getResources().getColor(R.color.white), SRC_IN);
            item.setIcon(drawable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_start_over){
            restartGame();
        }
        else if(item.getItemId() == R.id.action_finish_game){
            finishGame();
        }
        else if(item.getItemId() == R.id.action_rank){
            showRanks(false);
        }

        return true;
    }

    @Override
    public void onPause(){
        super.onPause();
        if(matchResultDisposable != null && !matchResultDisposable.isDisposed()){
            matchResultDisposable.dispose();
        }
    }

    private void initData(Bundle savedInstanceState) {
        Bundle bundle = savedInstanceState == null ? getArguments() : savedInstanceState;
        if(bundle != null){
            game = bundle.getParcelable(KEY_GAME);
            cards = bundle.getParcelableArrayList(KEY_CARDS);
            selectedCards = bundle.getParcelableArrayList(KEY_SELECTED_CARDS);
            score = bundle.getInt(KEY_SCORE);
            shouldRestoreNameInputDialog = bundle.getBoolean(KEY_RESTORE_NAME_INPUT_DIALOG);
            pendingMatchResult = bundle.getParcelable(KEY_PENDING_MATCH_RESULT);
        }

        if(selectedCards == null) selectedCards = new ArrayList<>();
    }

    private void setupPresenter() {
        presenter.setView(this);
    }

    private void setupView() {
        setupGameBoard();
    }

    private void setupGameBoard() {
        adapter = new GameAdapter();
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
        GridLayoutManager lm = new GridLayoutManager(getContext(), game.getColumn());
        recyclerView.setLayoutManager(lm);
        recyclerView.addItemDecoration(new SpaceItemDecoration(UIUtils.dip2px(getContext(), 5.0f), game.getColumn()));
    }

    private void onFinishGame(boolean hasWin) {
        shouldRestoreNameInputDialog = true;
        int titleRes = hasWin ? R.string.congratulations : R.string.game_finished;
        UIUtils.showTextFieldDialog(getContext(), titleRes, R.string.ask_name,
                text -> {
                    startLoading();
                    presenter.saveScore(text, score, game.getGameID());
                },
                v -> shouldRestoreNameInputDialog = false);
    }

    private void restartGame() {
        UIUtils.showYesNoDialog(getContext(), 0, R.string.restart_game_comfirm,
                0, sweetAlertDialog -> presenter.prepareCards(game),
                0, null);
    }

    private void finishGame() {
        UIUtils.showYesNoDialog(getContext(), 0, R.string.finish_game_comfirm,
                0, sweetAlertDialog -> onFinishGame(false),
                0, null);
    }

    private void showRanks(boolean fromFinishGame) {
        getHostFragment().push(RankFragment.newInstance(game, fromFinishGame));
    }

    private void updateScore(){
        setSubtitle(getString(R.string.current_score, score));
    }

    @Override
    public boolean onBackClicked() {
        UIUtils.showYesNoDialog(getContext(), 0, R.string.exit_game_comfirm,
                0, sweetAlertDialog -> getHostFragment().pop(),
                0, null);
        return true;
    }

    //GameView
    @Override
    public void onCardsPrepared(ArrayList<Card> cards) {
        this.cards = cards;
        adapter.setCards(cards);

        if(pendingMatchResult != null){
            processMatchResult();
        }
    }

    @Override
    public void onScoreSaved(ScoreRank rank) {
        if(!isAdded()) return;

        stopLoading();
        showRanks(true);
    }

    private void processMatchResult() {
        matchResultDisposable = Observable.just(pendingMatchResult)
                .delay(1200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(matchResult -> {
                    score += matchResult.getScore();
                    updateScore();
                    adapter.setMatchResult(matchResult, selectedCards);

                    if(matchResult.isMatching() && game.hasWin(cards)){
                        onFinishGame(true);
                    }
                })
                .subscribeWith(new SimpleDisposableObserver<MatchResult>() {
                    @Override
                    public void onNext(MatchResult matchResult) {
                        GameFragment.this.pendingMatchResult = null;
                    }
                });
    }

    //GameAdapterListener
    @Override
    public void onCardSelected(Card card) {
        selectedCards.add(card);
        score--;
        updateScore();

        if(selectedCards.size() == game.matchingCardsNumber()){
            pendingMatchResult =  game.matchCards(selectedCards);
            processMatchResult();
        }
    }

    @Override
    public void onCardDeselected(Card card) {
        selectedCards.remove(card);
    }

    @Override
    public boolean hasReachMaxSelectedCards() {
        return selectedCards != null && selectedCards.size() == game.matchingCardsNumber();
    }

}

package com.johnnie.cardplaying.ui.base;

/**
 * Created by Johnnie on 18/07/18.
 */

public interface ListItemClickedListener<T> {
    void onItemClicked(T item);
}

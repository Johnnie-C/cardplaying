package com.johnnie.cardplaying.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.presenter.BasePresenter;
import com.johnnie.cardplaying.utils.SingleToast;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import timber.log.Timber;

/**
 * Created by Johnnie on 18/07/18.
 */

public abstract class BaseFragment extends DaggerFragment implements BasePresenter.BaseView{

    private Unbinder viewUnbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);
        viewUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        _setupView(view);
        viewDidLoad(view, savedInstanceState);
    }

    @Override
    public void onStop(){
        super.onStop();
        stopLoading();
    }

    private void _setupView(View view) {
        View loader = view.findViewById(R.id.block_loader);
        if(loader != null){
            loader.setOnClickListener(v -> {});
        }
    }

    @Override
    public void onDestroyView() {
        if (viewUnbinder != null) viewUnbinder.unbind();

        super.onDestroyView();
    }

    protected abstract boolean hasOwnMenu();
    protected abstract int getLayoutRes();
    protected abstract void viewDidLoad(View rootView, Bundle savedInstanceState);

    protected BaseActivity getBaseActivity(){
        return (BaseActivity)getActivity();
    }

    @Override
    public void startLoading(String title){
        if(!isAdded()) return;

        View loader = getView().findViewById(R.id.block_loader);
        if(loader == null){
            Timber.e("%s doesn't have a loader", getClass().getSimpleName());
            return;
        }

        if(!TextUtils.isEmpty(title)){
            TextView txtTitle = loader.findViewById(R.id.txt_loader_title);
            txtTitle.setText(title);
        }

        loader.setVisibility(View.VISIBLE);
        loader.bringToFront();
    }

    protected void startLoading(){
        startLoading(null);
    }

    @Override
    public void stopLoading(){
        if(!isAdded()) return;

        View loader = getView().findViewById(R.id.block_loader);
        if(loader == null){
            Timber.e("%s doesn't have a loader", getClass().getSimpleName());
            return;
        }

        loader.setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String errorMsg) {
        SingleToast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
    }

    public void showErrorMessage(int stringRes) {
        SingleToast.makeText(getContext(), stringRes, Toast.LENGTH_SHORT).show();
    }

}

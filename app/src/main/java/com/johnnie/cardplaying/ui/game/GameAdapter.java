package com.johnnie.cardplaying.ui.game;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.card.Card;
import com.johnnie.cardplaying.modle.game.MatchResult;
import com.johnnie.cardplaying.ui.base.BaseRecyclerViewAdapter;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Johnnie on 18/07/18.
 */

public class GameAdapter extends BaseRecyclerViewAdapter<Card> {
    private ArrayList<Card> cards;
    private GameAdapterListener listener;
    private WeakReference<RecyclerView> recyclerViewWeakReference;

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
        notifyDataSetChanged();
    }

    public void setListener(GameAdapterListener listener) {
        this.listener = listener;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerViewWeakReference = new WeakReference<>(recyclerView);
    }

    @Override
    public int getItemCount() {
        return cards == null ? 0 : cards.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        return new CardCell(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CardCell) {
            CardCell cell = (CardCell) holder;
            final Card card = cards.get(position);

            updateCardUI(cell, card);

            cell.itemView.setOnClickListener(v -> {
                if(listener == null || listener.hasReachMaxSelectedCards()) return;
                card.setReveal(!card.hasRevealed());

                if(listener != null){
                    if(card.hasRevealed()){
                        if(listener != null) listener.onCardSelected(card);
                    }
                    else{
                        if(listener != null) listener.onCardDeselected(card);
                    }
                }

                flipCard(cell, card);
            });
        }
    }

    private void updateCardUI(CardCell cell, Card card){
        cell.ivBG.setImageDrawable(null);
        cell.txtTitle.setText(card.hasRevealed() ? card.getDisplayStr() : "");
        cell.txtTitle.setTextColor(cell.txtTitle.getContext().getResources().getColor(card.getDisplayStrTextColorRes()));
        cell.disableLayer.setVisibility(card.hasMatched() ? View.VISIBLE : View.GONE);

        int drawableRes = card.hasRevealed() ? R.drawable.card_front_bg : R.drawable.card_back;
        Picasso.with(cell.ivBG.getContext())
                .load(drawableRes)
                .fit()
                .into(cell.ivBG);
    }

    private void flipCard(CardCell cell, Card card) {
        Animation startToMiddle = AnimationUtils.loadAnimation(cell.itemView.getContext(), R.anim.to_middle);
        Animation middleToEnd = AnimationUtils.loadAnimation(cell.itemView.getContext(), R.anim.from_middle);

        Animation.AnimationListener listener = new Animation.AnimationListener() {
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationStart(Animation animation) {}

            @Override public void onAnimationEnd(Animation animation) {
                if (animation == startToMiddle) {
                    updateCardUI(cell, card);
                    cell.itemView.setAnimation(middleToEnd);
                    cell.itemView.startAnimation(middleToEnd);
                }
            }
        };
        startToMiddle.setAnimationListener(listener);
        middleToEnd.setAnimationListener(listener);

        cell.itemView.setAnimation(startToMiddle);
        cell.itemView.startAnimation(startToMiddle);
    }

    public void setMatchResult(MatchResult matchResult, ArrayList<Card> selectedCards) {
        Card lastSelectedCard = selectedCards.get(selectedCards.size() - 1);
        RecyclerView recyclerView = recyclerViewWeakReference == null ? null : recyclerViewWeakReference.get();

        for(Card card : selectedCards){
            //update card status
            card.setMatched(matchResult.isMatching());
            boolean shouldReveal = matchResult.isMatching() || card == selectedCards.get(selectedCards.size() - 1);
            card.setReveal(shouldReveal);

            if(recyclerView != null && !matchResult.isMatching() && card != lastSelectedCard){
                //if not matched, closed selected cards except last selected one

                RecyclerView.ViewHolder vh = recyclerView.findViewHolderForLayoutPosition(cards.indexOf(card));
                if(vh != null) {
                    //if card visible, show flip animation
                    flipCard((CardCell)vh, card);
                }
            }
        }

        if(recyclerView != null && matchResult.isMatching()){
            //if matched, update data set
            //has issue if call notifyItemChanged(position); on card individually
            //TODO: figure out why
            notifyDataSetChanged();

        }

        //clear selected card if need
        selectedCards.clear();
        if(!matchResult.isMatching()){
            selectedCards.add(lastSelectedCard);
        }

    }






    class CardCell extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_title) TextView txtTitle;
        @BindView(R.id.iv_bg) ImageView ivBG;
        @BindView(R.id.disable_layer) View disableLayer;

        public CardCell(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            disableLayer.setOnClickListener(v -> {});
        }
    }



    public interface GameAdapterListener{
        void onCardSelected(Card card);
        void onCardDeselected(Card card);
        boolean hasReachMaxSelectedCards();
    }

}

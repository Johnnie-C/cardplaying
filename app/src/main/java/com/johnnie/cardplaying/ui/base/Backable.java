package com.johnnie.cardplaying.ui.base;

/**
 * Created by Johnnie on 18/07/18.
 */

/**
 * if you want a fragment to hand hardware back button press event
 * implement this
 */
public interface Backable {

    /**
     * if response to back clicked event
     * @return true if consume back event
     */
    boolean onBackClicked();
}

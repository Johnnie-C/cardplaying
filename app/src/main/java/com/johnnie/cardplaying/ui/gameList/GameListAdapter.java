package com.johnnie.cardplaying.ui.gameList;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.game.Game;
import com.johnnie.cardplaying.ui.base.BaseRecyclerViewAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Johnnie on 18/07/18.
 */

public class GameListAdapter extends BaseRecyclerViewAdapter<Game> {

    private ArrayList<Game> games;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game, parent, false);
        return new CardCell(view);
    }

    public void setGames(ArrayList<Game> games) {
        this.games = games;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CardCell) {
            CardCell cell = (CardCell) holder;
            Game game = games.get(position);
            cell.txtTitle.setText(game.getNameRes());
            cell.itemView.setOnClickListener(v -> {
                if(itemClickedListener != null){
                    itemClickedListener.onItemClicked(game);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return games == null ? 0 : games.size();
    }


    public class CardCell extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_title) TextView txtTitle;

        public CardCell(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

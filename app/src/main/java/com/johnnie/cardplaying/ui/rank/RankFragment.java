package com.johnnie.cardplaying.ui.rank;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.ScoreRank;
import com.johnnie.cardplaying.modle.game.Game;
import com.johnnie.cardplaying.presenter.RankPresenter;
import com.johnnie.cardplaying.ui.base.Backable;
import com.johnnie.cardplaying.ui.base.BaseFeatureFragment;
import com.johnnie.cardplaying.ui.widget.JCRefreshLayout;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by Johnnie on 19/07/18.
 */

public class RankFragment extends BaseFeatureFragment implements RankPresenter.RankView,
        Backable{

    private final static String KEY_RANKS = "key_ranks";
    private final static String KEY_GAME = "key_game";
    private final static String KEY_ENTER_FROM_WIN = "key_enterFromWin";

    @BindView(R.id.header) View header;
    @BindView(R.id.refresh_layout) JCRefreshLayout refreshLayout;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    private RanksAdapter adapter;

    @Inject RankPresenter presenter;

    private ArrayList<ScoreRank> ranks;
    private Game game;
    private boolean isEnterFromWin;


    public static RankFragment newInstance(Game game, boolean isEnterFromWin){
        RankFragment fragment = new RankFragment();

        Bundle args = new Bundle();
        args.putParcelable(KEY_GAME, game);
        args.putBoolean(KEY_ENTER_FROM_WIN, isEnterFromWin);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_score_rank;
    }

    @Override
    protected void viewDidLoad(View rootView, Bundle savedInstanceState){
        super.viewDidLoad(rootView, savedInstanceState);
        initData(savedInstanceState);
        setupPresenter();
        setupView();

        if(ranks == null || ranks.size() <= 0){
            loadRanks();
        }
        else{
            onRanksLoad(ranks);
        }
    }

    @Override
    protected void onToolbarReady(@Nullable Toolbar toolbar){
        if(toolbar == null) return;

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(view -> getActivity().onBackPressed());
        setTitle(R.string.top_score);
        setSubtitle(game.getNameRes());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_RANKS, ranks);
        outState.putParcelable(KEY_GAME, game);
        outState.putBoolean(KEY_ENTER_FROM_WIN, isEnterFromWin);
    }

    private void loadRanks() {
        startLoading();
        presenter.loadRanks(game.getGameID(), 20);
    }

    private void initData(Bundle savedInstanceState) {
        Bundle bundle = savedInstanceState == null ? getArguments() : savedInstanceState;
        if(bundle != null){
            game = bundle.getParcelable(KEY_GAME);
            ranks = bundle.getParcelableArrayList(KEY_RANKS);
            isEnterFromWin = bundle.getBoolean(KEY_ENTER_FROM_WIN);
        }
    }

    private void setupPresenter() {
        presenter.setView(this);
    }

    private void setupView() {
        refreshLayout.setOnRefreshListener(this::loadRanks);
        adapter = new RanksAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    //RankView
    @Override
    public void onRanksLoad(ArrayList<ScoreRank> ranks) {
        if(!isAdded()) return;

        stopLoading();
        this.ranks = ranks;
        adapter.setRanks(ranks);
        header.setVisibility(ranks == null || ranks.size() == 0 ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void startLoading(){
        refreshLayout.startLoading();
    }

    @Override
    public void stopLoading(){
        refreshLayout.stopLoading();
    }

    @Override
    public boolean onBackClicked() {
        if(isEnterFromWin){
            getHostFragment().popToRoot();
        }
        else{
            getHostFragment().pop();
        }
        return true;
    }
}

package com.johnnie.cardplaying.ui.gameList;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.game.Game;
import com.johnnie.cardplaying.presenter.GameListPresenter;
import com.johnnie.cardplaying.ui.base.BaseFeatureFragment;
import com.johnnie.cardplaying.ui.game.GameFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by Johnnie on 18/07/18.
 */

public class GameListFragment extends BaseFeatureFragment implements GameListPresenter.GameListView {

    private final static String KEY_GAMES = "key_games";

    @BindView(R.id.recycler_view) RecyclerView recyclerView;
    private GameListAdapter adapter;

    @Inject GameListPresenter presenter;
    private ArrayList<Game> games;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_game_list;
    }

    @Override
    protected void viewDidLoad(View rootView, Bundle savedInstanceState){
        super.viewDidLoad(rootView, savedInstanceState);
        initData(savedInstanceState);
        setupPresenter();
        setupView();

        if(games == null || games.size() <= 0){
            startLoading();
            presenter.loadSupportedGames();
        }
        else{
            onGamesLoad(games);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        setTitle(R.string.game_list_title);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(KEY_GAMES, games);
    }

    private void setupPresenter() {
        presenter.setView(this);
    }

    private void initData(Bundle savedInstanceState) {
        Bundle bundle = savedInstanceState == null ? getArguments() : savedInstanceState;
        if(bundle != null){
            games = bundle.getParcelableArrayList(KEY_GAMES);
        }
    }

    private void setupView() {
        adapter = new GameListAdapter();
        adapter.setItemClickedListener(this::onGameClicked);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void onGameClicked(Game game) {
        getHostFragment().push(GameFragment.newInstance(game));
    }


    //GameListView
    @Override
    public void onGamesLoad(ArrayList<Game> games) {
        if(!isAdded()) return;

        stopLoading();
        this.games = games;
        adapter.setGames(games);
    }

}

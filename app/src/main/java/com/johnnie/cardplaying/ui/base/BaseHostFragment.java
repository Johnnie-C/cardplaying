package com.johnnie.cardplaying.ui.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.johnnie.cardplaying.R;

import butterknife.BindView;

/**
 * Created by Johnnie on 18/07/18.
 */

/**
 * Host fragment has a toolbar
 * equals to UINavigationController in iOS
 */
public abstract class BaseHostFragment extends BaseFragment implements Backable{

    private static final String TAG_CURRENT_FRAGMENT = "tag_currentFragment";
    private static final String TAG_ROOT_FRAGMENT = "tag_rootFragment";
    private static final String KEY_ROOT_FRAGMENT_ID = "key_rootFragmentID";

    private int rootFragmentID;

    @BindView(R.id.toolbar) Toolbar toolbar;

    @Override
    protected int getLayoutRes(){
        return R.layout.base_host_fragment;
    }

    @Override
    protected boolean hasOwnMenu() {
        return false;
    }

    @Override
    protected void viewDidLoad(View rootView, Bundle savedInstanceState){
        getBaseActivity().setSupportActionBar(toolbar);

        initData(savedInstanceState);
        if(savedInstanceState == null){
            setupLandingFragment();
        }
    }

    protected abstract Fragment getLandingFragment();

    @CallSuper
    protected void initData(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            rootFragmentID = savedInstanceState.getInt(KEY_ROOT_FRAGMENT_ID);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_ROOT_FRAGMENT_ID, rootFragmentID);
    }

    protected void setupLandingFragment() {
        Fragment fragment = getLandingFragment();
        replace(fragment, TAG_ROOT_FRAGMENT);
    }


    public void push(Fragment fragment){
        getChildFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out,
                        R.anim.slide_left_in, R.anim.slide_right_out)
                .replace(R.id.feature_container, fragment, TAG_CURRENT_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    public int replace(Fragment fragment){
        return replace(fragment, TAG_CURRENT_FRAGMENT);
    }

    private int replace(Fragment fragment, String tag){
        return getChildFragmentManager().beginTransaction()
                .replace(R.id.feature_container, fragment, tag)
                .commit();
    }

    public boolean pop(){
        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            getChildFragmentManager().popBackStackImmediate();
            return true;
        }
        return false;
    }

    public boolean popToRoot(){
        return getChildFragmentManager()
                .popBackStackImmediate(rootFragmentID , FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public boolean onBackClicked() {
        boolean hasHandle = false;

        if (getChildFragmentManager().getBackStackEntryCount() > 0) {
            Fragment currentFragment = getChildFragmentManager().findFragmentByTag(TAG_CURRENT_FRAGMENT);
            if(currentFragment instanceof Backable){
                hasHandle =((Backable)currentFragment).onBackClicked();
            }

            if(!hasHandle){
                hasHandle = pop();
            }
        }


        return hasHandle;
    }
}

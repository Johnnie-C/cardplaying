package com.johnnie.cardplaying.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.johnnie.cardplaying.ui.base.BaseHostFragment;

/**
 * Created by Johnnie on 20/07/18.
 */

public class SimpleHostFragment extends BaseHostFragment {

    private static final String KEY_LANDING_FRAGMENT_CLASS_NAME = "key_landingFragmentClassName";

    String landingFragmentClassName;

    public static SimpleHostFragment hostFragmentWithRootFragment(Class c){
        SimpleHostFragment hostFragment = new SimpleHostFragment();
        Bundle args = new Bundle();
        args.putString(KEY_LANDING_FRAGMENT_CLASS_NAME, c.getName());
        hostFragment.setArguments(args);

        return hostFragment;
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        super.initData(savedInstanceState);
        Bundle bundle = savedInstanceState == null ? getArguments() : savedInstanceState;

        if(bundle != null){
            landingFragmentClassName = bundle.getString(KEY_LANDING_FRAGMENT_CLASS_NAME);
        }
    }

    @Override
    protected Fragment getLandingFragment() {
        return Fragment.instantiate(getContext(), landingFragmentClassName);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_LANDING_FRAGMENT_CLASS_NAME, landingFragmentClassName);
    }
}

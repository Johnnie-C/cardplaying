package com.johnnie.cardplaying.ui;

import android.os.Bundle;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.ui.base.BaseActivity;
import com.johnnie.cardplaying.ui.gameList.GameListFragment;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if(savedInstanceState == null) {
            setupLandingFragment();
        }
    }

    private void setupLandingFragment() {
        replace(SimpleHostFragment.hostFragmentWithRootFragment(GameListFragment.class));
    }
}

package com.johnnie.cardplaying.di.module;

import android.app.Activity;

import com.johnnie.cardplaying.ui.MainActivity;
import com.johnnie.cardplaying.ui.SimpleHostFragment;
import com.johnnie.cardplaying.ui.game.GameFragment;
import com.johnnie.cardplaying.ui.gameList.GameListFragment;
import com.johnnie.cardplaying.ui.rank.RankFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Johnnie on 18/07/18.
 */

@Module()
public abstract class MainActivityModule {

    @Binds
    abstract Activity bindActivity(MainActivity mainActivity);

    //base host fragment
    @ContributesAndroidInjector abstract SimpleHostFragment simpleHostFragment();

    @ContributesAndroidInjector abstract GameListFragment gameListFragment();
    @ContributesAndroidInjector abstract GameFragment gameFragment();
    @ContributesAndroidInjector abstract RankFragment rankFragment();

}

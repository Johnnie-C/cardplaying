package com.johnnie.cardplaying.di.module;


import com.johnnie.cardplaying.di.qualifier.ActivityContext;
import com.johnnie.cardplaying.ui.MainActivity;
import com.johnnie.cardplaying.ui.base.BaseActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Johnnie on 18/07/18.
 *
 */
@Module
public abstract class ActivityBindingModule {

    @Binds
    @ActivityContext
    abstract BaseActivity bindActivity(BaseActivity activity);


    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivity();
}

package com.johnnie.cardplaying.di.module;

import android.app.Application;
import android.content.Context;

import com.johnnie.cardplaying.GameCenter;
import com.johnnie.cardplaying.di.qualifier.ApplicationContext;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Johnnie on 18/07/18
 */
@Module
public abstract class AppModule {

    @Binds
    @ApplicationContext
    abstract Context bindContext(Application application);

    @Provides
    public static GameCenter provideGameCenter(){
        return GameCenter.getInstance();
    }
}

package com.johnnie.cardplaying.db;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.johnnie.cardplaying.modle.ScoreRank;

import java.sql.SQLException;

import javax.inject.Inject;

/**
 * Created by Johnnie on 19/07/18.
 */

public class CPDbHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "cardPlaying.db";
    private static final int DATABASE_VERSION = 1;

    @Inject
    public CPDbHelper(Activity activity) {
        super(activity, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, ScoreRank.class);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try{
            TableUtils.dropTable(connectionSource, ScoreRank.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}

package com.johnnie.cardplaying.interactor;

import com.google.common.collect.Lists;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;
import com.johnnie.cardplaying.db.CPDbHelper;
import com.johnnie.cardplaying.modle.ScoreRank;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Johnnie on 19/07/18.
 */

public class ScoreRankInteractor {

    private CPDbHelper dbHelper;
    private List<ScoreRank> ranks = null;

    @Inject
    public ScoreRankInteractor(CPDbHelper dbHelper) {
        this.dbHelper = dbHelper;
        ranks = new ArrayList<>();
    }

    public Single<ArrayList<ScoreRank>> loadTopRanks(int gameID, long limit) {
        return Single.just(true)
                .observeOn(Schedulers.io())
                .map(ignore -> {
                    Dao<ScoreRank, Long> rankDao = dbHelper.getDao(ScoreRank.class);
                    ranks = rankDao.queryBuilder()
                            .limit(limit)
                            .orderBy(ScoreRank.COLUMN_NAME_SCORE, false)
                            .where()
                            .eq(ScoreRank.COLUMN_NAME_GAME_ID, gameID)
                            .query();
                    return Lists.newArrayList(ranks);
                });
    }

    public Single<ScoreRank> saveRank(final ScoreRank rank) {
        return Single.just(rank)
                .observeOn(Schedulers.io())
                .map(r -> {
                    final Dao<ScoreRank, Long> rankDao = dbHelper.getDao(ScoreRank.class);
                    rankDao.create(r);
                    return r;
                });
    }

    public void clearAllRanks() {
        try {
            Dao<ScoreRank, Long> rankDao = dbHelper.getDao(ScoreRank.class);
            TableUtils.clearTable(rankDao.getConnectionSource(), ScoreRank.class);
            ranks = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

package com.johnnie.cardplaying;

import com.johnnie.cardplaying.modle.game.Game;
import com.johnnie.cardplaying.modle.game.PlayingCardGame;
import com.johnnie.cardplaying.modle.game.SetGame;

import java.util.ArrayList;

import io.reactivex.Single;

/**
 * Created by Johnnie on 20/07/18.
 */

/**
 * For register a new game:
 *
 * create a game class extends from {@link Game<>}
 * The NewGame requires a gameID which must be unique to others in {@link com.johnnie.cardplaying.modle.game.GameID}
 * Add you gameID in {@link com.johnnie.cardplaying.modle.game.GameID} for future reference
 * and register you game in {@link GameCenter#generateSupportedGames()}
 */
public class GameCenter {

    private static GameCenter instance;
    private ArrayList<Game> games;

    /**
     * should be only called in DI {@link com.johnnie.cardplaying.di.module.AppModule}
     * use @Inject to get instance in other classes
     * @return
     */
    public static GameCenter getInstance(){
        if(instance == null){
            instance = new GameCenter();
        }

        return instance;
    }

    private GameCenter() {}

    public Single<ArrayList<Game>> getGames() {
        return Single.just(true)
                .map(ignore -> {
                    if(games == null || !games.isEmpty()){
                        generateSupportedGames();
                    }

                    return games;
                });
    }

    private void generateSupportedGames() {
        games = new ArrayList<>();
        games.add(new PlayingCardGame());
        games.add(new SetGame());
    }
}

package com.johnnie.cardplaying.presenter;

import com.johnnie.cardplaying.interactor.ScoreRankInteractor;
import com.johnnie.cardplaying.modle.ScoreRank;
import com.johnnie.cardplaying.modle.card.Card;
import com.johnnie.cardplaying.modle.game.Game;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Johnnie on 18/07/18.
 */

public class GamePresenter extends BasePresenter<GamePresenter.GameView>{

    private ScoreRankInteractor scoreRankInteractor;

    @Inject
    public GamePresenter(ScoreRankInteractor scoreRankInteractor) {
        this.scoreRankInteractor = scoreRankInteractor;
    }

    public void prepareCards(Game game){
        Single.just(true)
                .observeOn(Schedulers.io())
                .map(ignore -> game.generateCards())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cards -> getView().onCardsPrepared(cards),
                        onError);
    }

    public void saveScore(String name, int score, int gameID) {
        scoreRankInteractor.saveRank(new ScoreRank(name, score, gameID))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rank -> getView().onScoreSaved(rank),
                        onError);
    }

    public interface GameView extends BasePresenter.BaseView{
        void onCardsPrepared(ArrayList<Card> cards);
        void onScoreSaved(ScoreRank rank);
    }
}

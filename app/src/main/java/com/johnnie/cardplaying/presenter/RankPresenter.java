package com.johnnie.cardplaying.presenter;

import com.johnnie.cardplaying.interactor.ScoreRankInteractor;
import com.johnnie.cardplaying.modle.ScoreRank;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Johnnie on 19/07/18.
 */

public class RankPresenter extends BasePresenter<RankPresenter.RankView> {

    private ScoreRankInteractor scoreRankInteractor;

    @Inject
    public RankPresenter(ScoreRankInteractor scoreRankInteractor) {
        this.scoreRankInteractor = scoreRankInteractor;
    }

    public void loadRanks(int gameID, int limit) {
        scoreRankInteractor.loadTopRanks(gameID, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(ranks -> getView().onRanksLoad(ranks),
                        onError);
    }


    public interface RankView extends BasePresenter.BaseView{
        void onRanksLoad(ArrayList<ScoreRank> ranks);
    }
}

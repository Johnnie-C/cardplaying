package com.johnnie.cardplaying.presenter;

import com.johnnie.cardplaying.GameCenter;
import com.johnnie.cardplaying.modle.game.Game;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Johnnie on 18/07/18.
 */

public class GameListPresenter extends BasePresenter<GameListPresenter.GameListView>{

    @Inject GameCenter gameCenter;

    @Inject
    public GameListPresenter() {}

    public void loadSupportedGames(){
        gameCenter.getGames()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(games -> getView().onGamesLoad(games),
                        onError);
    }

    public interface GameListView extends BasePresenter.BaseView{
        void onGamesLoad(ArrayList<Game> games);
    }
}

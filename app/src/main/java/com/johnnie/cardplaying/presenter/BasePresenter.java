package com.johnnie.cardplaying.presenter;

import android.support.annotation.NonNull;

import com.johnnie.cardplaying.BuildConfig;

import io.reactivex.functions.Consumer;
import timber.log.Timber;

/**
 * Created by Johnnie on 18/07/18.
 */

public class BasePresenter<View extends BasePresenter.BaseView> {

    protected View view;

    public void setView(@NonNull View view) {
        this.view = view;
    }

    public View getView() {
        if(view == null){
            throw new RuntimeException("View is null, make sure setView() is called when initiates presenter");
        }
        return view;
    }

    protected final Consumer<? super Throwable> onError = (Consumer<Throwable>) throwable -> {
        if(BuildConfig.DEBUG) {
            Timber.e(throwable.getMessage());
            throwable.printStackTrace();
        }

        getView().stopLoading();
        getView().showErrorMessage(throwable.getMessage());
    };

    public interface BaseView{
        void startLoading(String title);
        void stopLoading();
        void showErrorMessage(String errorMsg);
    }
}

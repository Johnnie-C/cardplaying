package com.johnnie.cardplaying;

import com.johnnie.cardplaying.di.component.AppComponent;
import com.johnnie.cardplaying.di.component.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import timber.log.Timber;

/**
 * Created by Johnnie on 18/07/18.
 */

public class CPApplication extends DaggerApplication {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        if(appComponent == null){
            appComponent = DaggerAppComponent.builder()
                    .application(this)
                    .build();
        }

        appComponent.inject(this);

        return appComponent;
    }
}

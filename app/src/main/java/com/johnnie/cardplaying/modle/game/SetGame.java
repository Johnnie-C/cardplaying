package com.johnnie.cardplaying.modle.game;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.collect.Sets;
import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.card.SetCard;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Johnnie on 18/07/18.
 */


public class SetGame extends Game<SetCard>{

    public SetGame() {
        super(R.string.set_game_name);
    }

    @Override
    public int getGameID() {
        return GameID.GAME_ID_SET;
    }

    @Override
    public int getRow() {
        return 10;
    }

    @Override
    public int getColumn() {
        return 3;
    }

    @Override
    public int matchingCardsNumber() {
        return 3;
    }

    @Override
    public int totalCardInDeck() {
        return 81;
    }

    @Override
    protected ArrayList<SetCard> generateDeck() {
        ArrayList<SetCard> deck = new ArrayList<>();
        int[] colors = new int[]{SetCard.Color.RED, SetCard.Color.GREEN, SetCard.Color.PURPLE};
        String[] numbers = new String[]{SetCard.Number.ONE,SetCard. Number.TWO, SetCard.Number.THREE};
        String[] shadings = new String[]{SetCard.Shading.SOLID, SetCard.Shading.STRIPED, SetCard.Shading.OPEN};
        String[] symbols = new String[]{SetCard.Symbol.DIAMOND, SetCard.Symbol.SQUIGGLE, SetCard.Symbol.OVAL};


        for (int color : colors) {
            for (String number : numbers) {
                for (String shading : shadings) {
                    for (String symbol : symbols) {
                        deck.add(new SetCard(number, shading, symbol, color));
                    }
                }
            }
        }

        return deck;
    }


    @Override
    protected MatchResult hasPotentialToMatch(ArrayList<SetCard> cards) {
        if(cards == null || cards.size() <= 0){
            return new MatchResult(false, 0);
        }

        String[] numbers = new String[cards.size()];
        String[] symbols = new String[cards.size()];
        String[] shadings = new String[cards.size()];
        int[] colors = new int[cards.size()];

        for(int i = 0; i < cards.size(); i++){
            numbers[i] = cards.get(i).getNumber();
            symbols[i] = cards.get(i).getSymbol();
            shadings[i] = cards.get(i).getShading();
            colors[i] = cards.get(i).getColor();
        }

        boolean isMatch = allObjectSameOrAllUnique(numbers)
                && allObjectSameOrAllUnique(symbols)
                && allObjectSameOrAllUnique(shadings)
                && allObjectSameOrAllUnique(colors);

        return new MatchResult(isMatch, isMatch ? 16 : -2);
    }

    /**
     *
     * @param objects
     * @return true if (all object are same) || (all objects are different to each other)
     */
    private boolean allObjectSameOrAllUnique(Object... objects){
        if(objects == null || objects .length <= 0){
            return false;
        }

        HashSet set = Sets.newHashSet(objects);
        return set.size() == 1 || set.size() == objects.length;
    }







    protected SetGame(Parcel in) {
        super(in);
    }

    public static final Parcelable.Creator<SetGame> CREATOR = new Parcelable.Creator<SetGame>() {
        @Override
        public SetGame createFromParcel(Parcel source) {
            return new SetGame(source);
        }

        @Override
        public SetGame[] newArray(int size) {
            return new SetGame[size];
        }
    };
}

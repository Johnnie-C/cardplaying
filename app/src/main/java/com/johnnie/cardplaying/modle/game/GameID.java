package com.johnnie.cardplaying.modle.game;

import android.support.annotation.IntDef;

/**
 * Created by Johnnie on 19/07/18.
 */

@IntDef({GameID.GAME_ID_PLAYING_CARD,
        GameID.GAME_ID_SET,})
public @interface GameID{
    int GAME_ID_PLAYING_CARD = 1;
    int GAME_ID_SET = 2;
}

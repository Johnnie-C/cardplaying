package com.johnnie.cardplaying.modle.game;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Johnnie on 18/07/18.
 */

public class MatchResult implements Parcelable {
    private boolean isMatching;
    private int score;

    public MatchResult(boolean isMatching, int score) {
        this.isMatching = isMatching;
        this.score = score;
    }

    public boolean isMatching() {
        return isMatching;
    }

    public int getScore() {
        return score;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isMatching ? (byte) 1 : (byte) 0);
        dest.writeInt(this.score);
    }

    protected MatchResult(Parcel in) {
        this.isMatching = in.readByte() != 0;
        this.score = in.readInt();
    }

    public static final Parcelable.Creator<MatchResult> CREATOR = new Parcelable.Creator<MatchResult>() {
        @Override
        public MatchResult createFromParcel(Parcel source) {
            return new MatchResult(source);
        }

        @Override
        public MatchResult[] newArray(int size) {
            return new MatchResult[size];
        }
    };
}

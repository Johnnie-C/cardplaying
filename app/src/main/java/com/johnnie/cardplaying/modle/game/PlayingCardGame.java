package com.johnnie.cardplaying.modle.game;

import android.os.Parcel;

import com.johnnie.cardplaying.R;
import com.johnnie.cardplaying.modle.card.PokerCard;

import java.util.ArrayList;

/**
 * Created by Johnnie on 18/07/18.
 */

public class PlayingCardGame extends Game<PokerCard> {

    public PlayingCardGame() {
        super(R.string.play_card_game_name);
    }

    @Override
    public int getGameID() {
        return GameID.GAME_ID_PLAYING_CARD;
    }

    @Override
    public int getRow() {
        return 4;
    }

    @Override
    public int getColumn() {
        return 4;
    }

    @Override
    public int matchingCardsNumber() {
        return 2;
    }

    @Override
    public int totalCardInDeck() {
        return 52;
    }

    @Override
    protected MatchResult hasPotentialToMatch(ArrayList<PokerCard> cards) {
        PokerCard c1 = cards.get(0);
        PokerCard c2 = cards.get(1);

        boolean isMatch;
        int score;

        if(c1 == null || c2 == null){
            isMatch = false;
            score = -2;
        }
        else if(c1.getRank() == c2.getRank()){
            isMatch = true;
            score = 16;
        }
        else if(c1.getSuit().equalsIgnoreCase(c2.getSuit())){
            isMatch = true;
            score = 4;
        }
        else{
            isMatch = false;
            score = -2;
        }

        return new MatchResult(isMatch, score);
    }

    @Override
    protected ArrayList<PokerCard> generateDeck() {
        ArrayList<PokerCard> deck = new ArrayList<>();
        String[] suits = new String[]{PokerCard.Suit.SUIT_CLUB,
                PokerCard.Suit.SUIT_DIAMOND,
                PokerCard.Suit.SUIT_HEART,
                PokerCard.Suit.SUIT_SPADES};

        for (String suit : suits) {
            for (int j = 1; j <= 13; j++) {
                deck.add( new PokerCard(suit, j));
            }
        }

        return deck;
    }






    protected PlayingCardGame(Parcel in) {
        super(in);
    }

    public static final Creator<PlayingCardGame> CREATOR = new Creator<PlayingCardGame>() {
        @Override
        public PlayingCardGame createFromParcel(Parcel source) {
            return new PlayingCardGame(source);
        }

        @Override
        public PlayingCardGame[] newArray(int size) {
            return new PlayingCardGame[size];
        }
    };
}

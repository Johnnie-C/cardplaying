package com.johnnie.cardplaying.modle.game;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import com.google.common.collect.Lists;
import com.johnnie.cardplaying.modle.card.Card;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Johnnie on 18/07/18.
 */

/**
 *
 * @param <C> the Card used for game
 */
public abstract class Game<C extends Card> implements Parcelable {


    protected int nameRes;

    public Game(@StringRes int name) {
        this.nameRes = name;
    }

    public int getNameRes() {
        return nameRes;
    }

    /**
     * each game must have an ID
     * If you are creating a new game,
     * register your unique game id in {@link GameID}, and return the id in the implemented method
     * @return
     */
    public abstract @GameID int getGameID();

    public abstract int getRow();
    public abstract int getColumn();
    public abstract int matchingCardsNumber();
    public abstract int totalCardInDeck();
    protected abstract ArrayList<C> generateDeck();

    /**
     * this method only check if given cards are able to match potentially, or already matched.
     *
     * e.g.  a straight requires 5 cards
     * given cards [3, 4, 5], [3, 7], [4, 5, 6, 7] or [4, 5, 6, 8] should return true in {@link MatchResult}, as these cards are able to make straight with other future cards.
     * given cards [2, 3, 4, 5, 6] will return true in {@link MatchResult} as will
     *
     * but given cards [3, 9] or [2, 4, 5, 7] will return false in {@link MatchResult}
     *
     * @param cards
     * @return true in {@link MatchResult} if given cards are able to match potentially, or already matched. false otherwise
     */
    abstract protected MatchResult hasPotentialToMatch(ArrayList<C> cards);

    /**
     * the actual logic to check if cards are matched
     * This method is only called when cards.size() == {@link Game#matchingCardsNumber()}
     *
     * Normally, this logic will be same as {@link Game#hasPotentialToMatch(ArrayList)}
     * Override this if there is a really complex case
     *
     * @param cards
     * @return
     */
    protected MatchResult matchCardInternal(ArrayList<C> cards){
        return hasPotentialToMatch(cards);
    }


    /**
     * generate cards for game,
     * cards number equals to {@link #getRow()} * {@link #getColumn()}
     * generated cards must be a completable game.
     * @return
     */
    public ArrayList<Card> generateCards() {
        ArrayList<Card> cards = new ArrayList<>();

        int totalCount = getRow() * getColumn();
        ArrayList<C> deck = generateDeck();
        Collections.shuffle(deck);

        //ArrayList of Groups
        //each group contains matched cards are potentially become a matched set
        ArrayList<ArrayList<C>> groupedCard = new ArrayList<>();

        for(C card : deck){
            if(cards.size() == totalCount){//cards for games all generated
                break;
            }

            ArrayList<C> group = findFirstGroupForCard(groupedCard, card);
            if(group.size() == matchingCardsNumber()){
                groupedCard.remove(group);
                cards.addAll(group);
            }
        }

        Collections.shuffle(cards);
        return cards;
    }

    private ArrayList<C> findFirstGroupForCard(ArrayList<ArrayList<C>> pickedCards, C card) {
        ArrayList<C> matchCards = null;

        for(ArrayList<C> stack : pickedCards){
            ArrayList<C> cardsToMatch = Lists.newArrayList(stack);
            cardsToMatch.add(card);

            //call hasPotentialToMatch() rather than matchCards() to skip count check
            //here does not checking if cardsToMatch are exactly match the game rule
            //here only check if they can potentially match in future
            MatchResult matchResult = hasPotentialToMatch(cardsToMatch);

            if(matchResult != null && matchResult.isMatching()){
                matchCards = stack;
                break;
            }
        }

        if(matchCards == null){
            matchCards = new ArrayList<>();
            pickedCards.add(matchCards);
        }

        matchCards.add(card);

        return matchCards;
    }

    public MatchResult matchCards(ArrayList<C> cards){
        MatchResult matchResult = null;
        if(cards != null && cards.size() == matchingCardsNumber()){
            matchResult = matchCardInternal(cards);
        }
        else{
            matchResult = new MatchResult(false, 0);
        }

        return matchResult;
    }

    public boolean hasWin(ArrayList<C> cards){
        boolean hasWin = true;

        for(C card : cards){
            if(!card.hasMatched()){
                hasWin = false;
                break;
            }
        }

        return hasWin;
    }






    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.nameRes);
    }

    protected Game(Parcel in) {
        this.nameRes = in.readInt();
    }

}

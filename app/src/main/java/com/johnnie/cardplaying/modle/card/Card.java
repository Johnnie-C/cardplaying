package com.johnnie.cardplaying.modle.card;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.ColorRes;

import com.johnnie.cardplaying.R;

/**
 * Created by Johnnie on 18/07/18.
 */

public abstract class Card implements Parcelable {

    private boolean hasRevealed;
    private boolean hasMatched;

    public abstract String getDisplayStr();

    /**
     * is card face up
     * @return
     */
    public boolean hasRevealed() {
        return hasRevealed;
    }

    public void setReveal(boolean hasRevealed) {
        this.hasRevealed = hasRevealed;
    }

    /**
     * is card has match with another in game
     * @return
     */
    public boolean hasMatched(){
        return hasMatched;
    }

    public void setMatched(boolean hasMatched){
        this.hasMatched = hasMatched;
    }

    public @ColorRes int getDisplayStrTextColorRes(){
        return R.color.text_title;
    }





    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public Card() {
    }

    protected Card(Parcel in) {
    }

}

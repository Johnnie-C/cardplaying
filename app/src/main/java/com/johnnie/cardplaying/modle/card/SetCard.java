package com.johnnie.cardplaying.modle.card;

import android.os.Parcel;
import android.support.annotation.ColorRes;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import com.johnnie.cardplaying.R;

import java.util.Locale;

/**
 * Created by Johnnie on 19/07/18.
 */

public class SetCard extends Card {

    @IntDef({Color.RED, Color.GREEN, Color.PURPLE})
    public @interface Color{
        int RED = R.color.red;
        int GREEN = R.color.green;
        int PURPLE = R.color.purple;
    }


    @StringDef({Number.ONE, Number.TWO, Number.THREE})
    public @interface Number {
        String ONE = "One";
        String TWO = "Two";
        String THREE = "Three";
    }


    @StringDef({Shading.SOLID, Shading.STRIPED, Shading.OPEN})
    public @interface Shading {
        String SOLID = "Solid";
        String STRIPED = "Striped";
        String OPEN = "Open";
    }

    @StringDef({Symbol.DIAMOND, Symbol.SQUIGGLE, Symbol.OVAL})
    public @interface Symbol {
        String DIAMOND = "Diamond";
        String SQUIGGLE = "Squiggle";
        String OVAL = "Oval";
    }








    private @Number String number;
    private @Shading String shading;
    private @Symbol String symbol;
    private @Color int color;

    public SetCard(@Number String number, @Shading String shading, @Symbol String symbol, @Color int color) {
        this.number = number;
        this.shading = shading;
        this.symbol = symbol;
        this.color = color;
    }

    @Override
    public String getDisplayStr() {
        return String.format(Locale.getDefault(),"%s\n%s\n%s",
                number, symbol, shading);
    }

    @Override
    public @ColorRes int getDisplayStrTextColorRes(){
        return color;
    }

    public String getNumber() {
        return number;
    }

    public String getShading() {
        return shading;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getColor() {
        return color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.number);
        dest.writeString(this.shading);
        dest.writeString(this.symbol);
        dest.writeInt(this.color);
    }

    protected SetCard(Parcel in) {
        super(in);
        this.number = in.readString();
        this.shading = in.readString();
        this.symbol = in.readString();
        this.color = in.readInt();
    }

    public static final Creator<SetCard> CREATOR = new Creator<SetCard>() {
        @Override
        public SetCard createFromParcel(Parcel source) {
            return new SetCard(source);
        }

        @Override
        public SetCard[] newArray(int size) {
            return new SetCard[size];
        }
    };
}

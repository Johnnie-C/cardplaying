package com.johnnie.cardplaying.modle;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Johnnie on 19/07/18.
 */

public class ScoreRank implements Comparable, Parcelable {
    public final static String COLUMN_NAME_SCORE = "score";
    public final static String COLUMN_NAME_GAME_ID = "game_id";

    @DatabaseField(generatedId = true)
    private long internal_id;

    @DatabaseField
    String name;

    @DatabaseField(columnName = COLUMN_NAME_SCORE)
    int score;

    @DatabaseField(columnName = COLUMN_NAME_GAME_ID)
    int gameID;


    public ScoreRank(String name, int score, int gameID) {
        this.name = name;
        this.score = score;
        this.gameID = gameID;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public int getGameID() {
        return gameID;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        if(o instanceof ScoreRank){
            return ((ScoreRank)o).score - score;
        }
        return 1;
    }


    public ScoreRank() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.internal_id);
        dest.writeString(this.name);
        dest.writeInt(this.score);
        dest.writeInt(this.gameID);
    }

    protected ScoreRank(Parcel in) {
        this.internal_id = in.readLong();
        this.name = in.readString();
        this.score = in.readInt();
        this.gameID = in.readInt();
    }

    public static final Creator<ScoreRank> CREATOR = new Creator<ScoreRank>() {
        @Override
        public ScoreRank createFromParcel(Parcel source) {
            return new ScoreRank(source);
        }

        @Override
        public ScoreRank[] newArray(int size) {
            return new ScoreRank[size];
        }
    };
}

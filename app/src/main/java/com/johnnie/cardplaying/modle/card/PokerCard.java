package com.johnnie.cardplaying.modle.card;

import android.os.Parcel;
import android.support.annotation.ColorRes;
import android.support.annotation.StringDef;

import com.johnnie.cardplaying.R;

/**
 * Created by Johnnie on 18/07/18.
 */

public class PokerCard extends Card {

    @StringDef({Suit.SUIT_CLUB, Suit.SUIT_DIAMOND, Suit.SUIT_HEART, Suit.SUIT_SPADES})
    public @interface Suit{
        String SUIT_CLUB = "♣";
        String SUIT_DIAMOND = "♦";
        String SUIT_HEART = "♥";
        String SUIT_SPADES = "♠";
    }





    private @Suit String suit;
    private int rank;

    public PokerCard(@Suit String suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public String getSuit() {
        return suit;
    }

    public int getRank() {
        return rank;
    }

    @Override
    public String getDisplayStr() {
        return String.format("%s %s", suit, rankDisplayStr());
    }

    private String rankDisplayStr(){
        String str = rank + "";

        switch (rank){
            case 1:
                str = "A";
                break;

            case 11:
                str = "J";
                break;

            case 12:
                str = "Q";
                break;

            case 13:
                str = "K";
                break;
        }

        return str;
    }

    @Override
    public @ColorRes int getDisplayStrTextColorRes(){
        int colorRes = R.color.text_title;
        if(suit.equalsIgnoreCase(Suit.SUIT_DIAMOND) || suit.equalsIgnoreCase(Suit.SUIT_HEART)){
            colorRes = R.color.red;
        }
        return colorRes;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.suit);
        dest.writeInt(this.rank);
    }

    protected PokerCard(Parcel in) {
        super(in);
        this.suit = in.readString();
        this.rank = in.readInt();
    }

    public static final Creator<PokerCard> CREATOR = new Creator<PokerCard>() {
        @Override
        public PokerCard createFromParcel(Parcel source) {
            return new PokerCard(source);
        }

        @Override
        public PokerCard[] newArray(int size) {
            return new PokerCard[size];
        }
    };
}

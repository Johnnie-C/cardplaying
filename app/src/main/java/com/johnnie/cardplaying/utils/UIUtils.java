package com.johnnie.cardplaying.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.StateSet;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.johnnie.cardplaying.R;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.util.Calendar;
import java.util.Date;


/**
 * Created by Johnnie on 18/07/18.
 */
public class UIUtils {


    public static void showYesNoDialog(Context context,
                                       int titleRes,
                                       int contentRes,
                                       int yesBtnTitleRes,
                                       @Nullable View.OnClickListener positiveListener,
                                       int noBtnTitleTes,
                                       @Nullable View.OnClickListener negativeListener)
    {
        if(yesBtnTitleRes == 0) yesBtnTitleRes = R.string.yes;
        if(noBtnTitleTes == 0) noBtnTitleTes = R.string.no;

        new LovelyStandardDialog(context, LovelyStandardDialog.ButtonLayout.HORIZONTAL)
                .setTopColorRes(R.color.cp_blue)
                .setIcon(R.drawable.ic_info_outline_white_36dp)
                .setTitle(titleRes == 0 ? "" : context.getString(titleRes))
                .setMessage(context.getString(contentRes))
                .setPositiveButton(yesBtnTitleRes, positiveListener != null ? positiveListener : v -> {})
                .setNegativeButton(noBtnTitleTes, negativeListener != null ? negativeListener : v -> {})
                .show();
    }

    public static void showConfirmDialog(Context context, String string, DialogInterface.OnClickListener positiveListener){
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setMessage(string)
                .setPositiveButton("OK", positiveListener != null ? positiveListener : (DialogInterface.OnClickListener) (dialog, which) -> {

                })
                .create();
        setDialogButtonThemeIfNeed(alertDialog);
        alertDialog.show();
    }

    public static void showSuccessDialog(Context context, int contentRes){
        showSuccessDialog(context, R.string.success, contentRes);
    }

    public static void showSuccessDialog(Context context, int titleRes, int contentRes){
        showSuccessDialog(context, titleRes, contentRes, null);
    }

    public static void showSuccessDialog(Context context, int titleRes, int contentRes, View.OnClickListener positiveListener){
        new LovelyStandardDialog(context, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.cp_blue)
                .setIcon(R.drawable.ic_check)
                .setTitle(titleRes == 0 ? "" : context.getString(titleRes))
                .setMessage(contentRes == 0 ? "" : context.getString(contentRes))
                .setPositiveButton(R.string.done, positiveListener != null ? positiveListener : v -> {})
                .setCancelable(false)
                .show();
    }

    public static void showTextFieldDialog(Context context, int titleRes, int contentRes,
                                           @NonNull LovelyTextInputDialog.OnTextInputConfirmListener positiveListener,
                                           @Nullable View.OnClickListener negativeListener){
        new LovelyTextInputDialog(context)
                .setTitle(titleRes == 0 ? "" : context.getString(titleRes))
                .setMessage(contentRes == 0 ? "" : context.getString(contentRes))
                .setConfirmButton(R.string.done, positiveListener)
                .setNegativeButton(R.string.cancel, negativeListener != null ? negativeListener : v -> {})
                .setCancelable(false)
                .show();
    }

    public static void setDialogButtonThemeIfNeed(final AlertDialog dialog){
        dialog.setOnShowListener(arg0 -> {
            int positiveBtnTextColor = dialog.getContext().getResources().getColor(R.color.cp_blue);
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(positiveBtnTextColor);

            int negativeBtnTextColor = dialog.getContext().getResources().getColor(R.color.black);
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(negativeBtnTextColor);
        });
    }

    public static int dip2px(Context context, float dpValue) {
        if(context == null)
        {
            return 0;
        }
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static boolean isPointInsideView(float x, float y, View view) {
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];

        // point is inside view bounds
        return ((x > viewX && x < (viewX + view.getWidth())) &&
                (y > viewY && y < (viewY + view.getHeight())));
    }

    public static boolean isAClick(float startX, float endX, float startY, float endY) {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        if (differenceX > 5 || differenceY > 5) {
            return false;
        }
        return true;
    }

    public static int darkerColor(int color){
        float factor = 0.8f;
        int a = Color.alpha(color);
        int r = Math.round(Color.red(color) * factor);
        int g = Math.round(Color.green(color) * factor);
        int b = Math.round(Color.blue(color) * factor);
        return Color.argb(a,
                Math.min(r, 255),
                Math.min(g, 255),
                Math.min(b, 255));
    }

    public static int lighterColor(int color){
        float factor = 1.2f;
        int a = Color.alpha(color);
        int r = Math.round(Color.red(color) * factor);
        int g = Math.round(Color.green(color) * factor);
        int b = Math.round(Color.blue(color) * factor);
        return Color.argb(a,
                Math.min(r, 255),
                Math.min(g, 255),
                Math.min(b, 255));
    }

    public static int scaleColor(int color, float scale){
        int a = Color.alpha(color);
        int r = Math.round(Color.red(color) * scale);
        int g = Math.round(Color.green(color) * scale);
        int b = Math.round(Color.blue(color) * scale);
        return Color.argb(a,
                Math.min(r, 255),
                Math.min(g, 255),
                Math.min(b, 255));
    }

    public static StateListDrawable getTouchableDrawable(int normalColor, int highlightColor){
        ColorDrawable normalDrawable = new ColorDrawable(normalColor);
        ColorDrawable highlightDrawable = new ColorDrawable(highlightColor);
        StateListDrawable cellBGDrawable = new StateListDrawable();
        cellBGDrawable.addState(new int[] {android.R.attr.state_pressed} ,highlightDrawable);
        cellBGDrawable.addState(StateSet.WILD_CARD ,normalDrawable);
        return cellBGDrawable;
    }

    public static StateListDrawable getTouchableDrawable(int normalColor, int highlightColor, int cornerRadius){
        GradientDrawable normalDrawable = new GradientDrawable();
        normalDrawable.setColor(normalColor);
        normalDrawable.setCornerRadius(cornerRadius);

        GradientDrawable highlightDrawable = new GradientDrawable();
        highlightDrawable.setColor(highlightColor);
        highlightDrawable.setCornerRadius(cornerRadius);

        StateListDrawable cellBGDrawable = new StateListDrawable();
        cellBGDrawable.addState(new int[] {android.R.attr.state_pressed} ,highlightDrawable);
        cellBGDrawable.addState(StateSet.WILD_CARD ,normalDrawable);
        return cellBGDrawable;
    }

    public static View presentView(Activity activity, Dialog dlg, View view){
        dlg.setContentView(view);
        Window window = dlg.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        WindowManager wm = activity.getWindowManager();
        int width = wm.getDefaultDisplay().getWidth();
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        lp.width = width;
        window.setAttributes(lp);
        dlg.show();
        return view;
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth() > 0 ? view.getWidth() : 1,
                view.getHeight() > 0 ? view.getHeight() : 1,
                Bitmap.Config.ARGB_8888);

        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public static void showDatePicker(Context context, Calendar cal, Date minDate, DatePickerDialog.OnDateSetListener listener){
        DatePickerDialog dialog = new DatePickerDialog(context,
                listener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

        if(minDate != null){
            dialog.getDatePicker().setMinDate(minDate.getTime());
        }

        dialog.show();
    }

    public static void showTimePicker(Context context, Calendar cal, TimePickerDialog.OnTimeSetListener listener){
        TimePickerDialog dialog = new TimePickerDialog(context,
                listener,
                cal.get(Calendar.HOUR),
                cal.get(Calendar.MINUTE),
                false);

        dialog.show();
    }

    public static void closeSoftKeyboard(View view) {
        try {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

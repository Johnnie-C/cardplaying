package com.johnnie.cardplaying.utils;

import com.johnnie.cardplaying.BuildConfig;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by Johnnie on 19/07/18.
 */

public abstract class SimpleDisposableObserver<T> extends DisposableObserver<T> {

    @Override
    public void onError(Throwable e) {
        if(BuildConfig.DEBUG){
            e.printStackTrace();
        }
    }

    @Override
    public void onComplete() {

    }

}

package com.johnnie.cardplaying.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;

import com.johnnie.cardplaying.R;


/**
 * Created by Johnnie on 18/07/18.
 */

public class IntentUtils {

    public static IntentResult sendEmail(Context context, String[] emails){
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/html");
            intent.putExtra(Intent.EXTRA_EMAIL, emails);
            context.startActivity(Intent.createChooser(intent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            return new IntentResult(context.getString(R.string.error_no_email_app));
        }

        return new IntentResult(null);
    }

    public static IntentResult callNumber(Context context, String phone, String ext){
        if(TextUtils.isEmpty(phone)) return new IntentResult(context.getString(R.string.error_no_invalid_phone));

        String fullNumber = phone;
        if(!TextUtils.isEmpty(ext)) fullNumber += PhoneNumberUtils.WAIT + ext;
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", fullNumber, null));
        context.startActivity(intent);
        return new IntentResult(null);
    }

    public static IntentResult openUrl(Context context, String url){
        if(!url.startsWith("http")) url = "http://" + url;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        try {
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            return new IntentResult(String.format(context.getString(R.string.error_cannot_open_url), url));
        }

        return new IntentResult(null);
    }





    public static class IntentResult{
        private String errorMsg;
        private boolean success;

        public IntentResult(String errorMsg) {
            this.errorMsg = errorMsg;
            success = TextUtils.isEmpty(errorMsg);
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public boolean isSuccess() {
            return success;
        }
    }


}

package com.johnnie.cardplaying;

import com.google.common.collect.Lists;
import com.johnnie.cardplaying.modle.card.Card;
import com.johnnie.cardplaying.modle.card.PokerCard;
import com.johnnie.cardplaying.modle.card.SetCard;
import com.johnnie.cardplaying.modle.game.MatchResult;
import com.johnnie.cardplaying.modle.game.PlayingCardGame;
import com.johnnie.cardplaying.modle.game.SetGame;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class SetGameTest {

    private SetGame game;
    private ArrayList<Card> cards;

    @Before
    public void setup(){
        game = new SetGame();
        cards = game.generateCards();
    }

    @Test
    public void testGenerateCard() throws Exception {
        Assert.assertNotNull(cards);
        Assert.assertEquals(game.getColumn() * game.getRow(), cards.size());
    }

    //TODO: too much test cases, find a better way

    @Test
    public void testMatch_DifferentNumber(){
        SetCard c1 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c2 = new SetCard(SetCard.Number.TWO, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c3 = new SetCard(SetCard.Number.THREE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2, c3));
        Assert.assertTrue(matchResult.isMatching());
        Assert.assertEquals(16, matchResult.getScore());
    }

    @Test
    public void testMatch_DifferentShading(){
        SetCard c1 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c2 = new SetCard(SetCard.Number.ONE, SetCard.Shading.SOLID , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c3 = new SetCard(SetCard.Number.ONE, SetCard.Shading.STRIPED , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2, c3));
        Assert.assertTrue(matchResult.isMatching());
        Assert.assertEquals(16, matchResult.getScore());
    }

    @Test
    public void testMatch_DifferentSymbol(){
        SetCard c1 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c2 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.OVAL, SetCard.Color.GREEN);
        SetCard c3 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.SQUIGGLE, SetCard.Color.GREEN);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2, c3));
        Assert.assertTrue(matchResult.isMatching());
        Assert.assertEquals(16, matchResult.getScore());
    }

    @Test
    public void testMatch_DifferentColor(){
        SetCard c1 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c2 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.RED);
        SetCard c3 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.PURPLE);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2, c3));
        Assert.assertTrue(matchResult.isMatching());
        Assert.assertEquals(16, matchResult.getScore());
    }

    @Test
    public void testMatch_NotMatch(){
        SetCard c1 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c2 = new SetCard(SetCard.Number.TWO, SetCard.Shading.SOLID , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c3 = new SetCard(SetCard.Number.ONE, SetCard.Shading.STRIPED , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2, c3));
        Assert.assertFalse(matchResult.isMatching());
        Assert.assertEquals(-2, matchResult.getScore());
    }


    @Test
    public void testMatch_tooLessCardNumber(){
        SetCard c1 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c2 = new SetCard(SetCard.Number.TWO, SetCard.Shading.SOLID , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1));
        Assert.assertFalse(matchResult.isMatching());


        matchResult = game.matchCards(Lists.newArrayList(c1, c2));
        Assert.assertFalse(matchResult.isMatching());
    }

    @Test
    public void testMatch_tooMuchCardNumber(){
        SetCard c1 = new SetCard(SetCard.Number.ONE, SetCard.Shading.OPEN , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c2 = new SetCard(SetCard.Number.TWO, SetCard.Shading.SOLID , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c3 = new SetCard(SetCard.Number.ONE, SetCard.Shading.STRIPED , SetCard.Symbol.DIAMOND, SetCard.Color.GREEN);
        SetCard c4 = new SetCard(SetCard.Number.ONE, SetCard.Shading.STRIPED , SetCard.Symbol.SQUIGGLE, SetCard.Color.GREEN);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2, c3, c4));
        Assert.assertFalse(matchResult.isMatching());
    }

    @Test
    public void testMatch_NoCard(){
        MatchResult matchResult = game.matchCards(null);
        Assert.assertFalse(matchResult.isMatching());


        matchResult = game.matchCards(new ArrayList<>());
        Assert.assertFalse(matchResult.isMatching());
    }

}
package com.johnnie.cardplaying;

import com.google.common.collect.Lists;
import com.johnnie.cardplaying.modle.card.Card;
import com.johnnie.cardplaying.modle.card.PokerCard;
import com.johnnie.cardplaying.modle.game.MatchResult;
import com.johnnie.cardplaying.modle.game.PlayingCardGame;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class PlayingCardGameTest {

    private PlayingCardGame game;
    private ArrayList<Card> cards;

    @Before
    public void setup(){
        game = new PlayingCardGame();
        cards = game.generateCards();
    }

    @Test
    public void testGenerateCard() throws Exception {
        Assert.assertNotNull(cards);
        Assert.assertEquals(game.getColumn() * game.getRow(), cards.size());
    }

    @Test
    public void testMatch_SuitMatch(){
        PokerCard c1 = new PokerCard(PokerCard.Suit.SUIT_CLUB, 1);
        PokerCard c2 = new PokerCard(PokerCard.Suit.SUIT_CLUB, 2);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2));
        Assert.assertTrue(matchResult.isMatching());
        Assert.assertEquals(4, matchResult.getScore());
    }

    @Test
    public void testMatch_RankMatch(){
        PokerCard c1 = new PokerCard(PokerCard.Suit.SUIT_CLUB, 1);
        PokerCard c2 = new PokerCard(PokerCard.Suit.SUIT_DIAMOND, 1);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2));
        Assert.assertTrue(matchResult.isMatching());
        Assert.assertEquals(16, matchResult.getScore());
    }

    @Test
    public void testMatch_tooLessCardNumber(){
        PokerCard c1 = new PokerCard(PokerCard.Suit.SUIT_CLUB, 1);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1));
        Assert.assertFalse(matchResult.isMatching());
    }

    @Test
    public void testMatch_tooMuchCardNumber(){
        PokerCard c1 = new PokerCard(PokerCard.Suit.SUIT_CLUB, 1);
        PokerCard c2 = new PokerCard(PokerCard.Suit.SUIT_DIAMOND, 2);
        PokerCard c3 = new PokerCard(PokerCard.Suit.SUIT_HEART, 3);

        MatchResult matchResult = game.matchCards(Lists.newArrayList(c1, c2, c3));
        Assert.assertFalse(matchResult.isMatching());
    }

    @Test
    public void testMatch_NoCard(){
        MatchResult matchResult = game.matchCards(null);
        Assert.assertFalse(matchResult.isMatching());


        matchResult = game.matchCards(new ArrayList<>());
        Assert.assertFalse(matchResult.isMatching());
    }

}